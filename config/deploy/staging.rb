set :branch, :master
set :rails_env, :staging
set :deploy_to, '/home/azureuser/apps/captesting'
set :user, :azureuser
set :nginx_server_name, 'erfrs-demo.cloudapp.net'

# set :linked_files, %w[config/database.yml]

server 'erfrs-demo.cloudapp.net', user: 'azureuser', roles: %w[web app]

set :ssh_options, {
  keys: ["/Users/ken/.ssh/erfrsUser.key"],
  forward_agent: true,
  auth_methods: %w[publickey]
}
